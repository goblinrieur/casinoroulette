# Very simple casino roulette 

Random value will be displayed on 7 segments display when you've pushed the button.

![schema](./cr/cr.pdf)

![PCB](./cr/PCB.pdf)

# pics

![dispatch](./pics/component_dispatch.png)

![copper side](./pics/PCB_copper.png)

![components](./pics/PCB_up.png)

![schema](./pics/schema_BW.png)
